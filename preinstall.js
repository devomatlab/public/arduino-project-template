const child_process = require('child_process');


console.log('Preinstall check');

// Check arduino-cli
const p = child_process.spawnSync('arduino-cli')
if (p.error) {
  console.error('\x1b[41m\x1b[37m%s\x1b[0m', '"arduino-cli" command required.');
  console.error('Please donwload arduino-cli and makesure it is available in command line.\n');
  process.exit(1);
}

console.log('Preinstall completed\n');
