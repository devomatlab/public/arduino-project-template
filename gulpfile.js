const argv = require('yargs').argv;
const buffer = require('vinyl-buffer');
const child = require('child_process');
const del = require('del');
const fs = require('fs');
const glob = require('glob');
const gulp = require('gulp');
const path = require('path');
const paths = require('vinyl-paths');
const plugins = require('gulp-load-plugins')({lazy: false});
const source = require('vinyl-source-stream');

const dir = {
  arduinoModules: './arduino_modules',
  arduino: './arduino',
  build: './build',
};

const env = {
  BIN_NAME: 'arduino.bin',
};

function handleError (task) {
  return function (err) {
    plugins.util.beep();
    plugins.util.log(
      plugins.util.colors.bgRed(task + ' error:'),
      plugins.util.colors.red(err)
    );
  };
}

function clean () {
  return gulp.src([
    `${dir.build}/*`,
  ])
    .pipe(plugins.print.default(p=> `del ${p}`))
    .pipe(paths(del));
}

function install(done) {
  let boards = fs.readFileSync('./arduino-boards.txt', 'utf8')
    .split('\n')
    .map(d=>d.trim())
    .filter(d=>!!d)
    .join(' ');

  let libraries = fs.readFileSync('./arduino-libraries.txt', 'utf8')
    .split('\n')
    .map(d=>d.trim())
    .filter(d=>!!d)
  let gitLibraries = libraries.filter(d=>d.startsWith('http') && d.endsWith('git')); //TODO: better url check
  let arduinoLibraries = libraries
    .filter(d=>!gitLibraries.includes(d))
    .join(' ');

  let cli = 'arduino-cli --config-file ./arduino-cli.json';
  let libDir = `${dir.arduinoModules}/arduino/libraries`;
  let cmd = [
    `${cli} core update-index && ${cli} core install ${boards}`,
    `${cli} lib update-index ${arduinoLibraries? '&& ' + cli + ' lib install ' + arduinoLibraries : ''}`,
    ...gitLibraries.map(d=>`(git -C ${libDir} clone ${d} || echo "Please update ${d} manually.")`), //TODO: pull if dictionary exist
  ];

  if (!fs.existsSync(libDir)) {
    fs.mkdirSync(libDir, {recursive:true});
  }
  let proc = child.exec(cmd.join(' && ')).on('close', done);
  let logCB = function (buf) {
    process.stdout.write(buf);
  }
  proc.stdout.on('data', logCB);
  proc.stderr.on('data', logCB);
}

function compile(done) {
  let cli = 'arduino-cli --config-file ./arduino-cli.json';
  let fqbn = '--fqbn esp8266:esp8266:nodemcuv2:xtal=80,vt=flash,exception=legacy,ssl=all,eesz=4M2M,led=2,ip=lm2f,dbg=Disabled,lvl=None____,wipe=none,baud=921600';
  let outputDir = path.resolve(dir.build + '/bin');
  let output = `--output ${outputDir}/${env.BIN_NAME} --build-path ${path.resolve(dir.arduinoModules + '/build_cache')}`;
  let cmd = `${cli} compile ${fqbn} ${output} ${dir.arduino}/arduino.ino`;

  if (!fs.existsSync(outputDir)) {
    fs.mkdirSync(outputDir, {recursive:true});
  }
  let proc = child.exec(cmd).on('close', done);
  let logCB = function (buf) {
    process.stdout.write(buf);
  }
  proc.stdout.on('data', logCB);
  proc.stderr.on('data', logCB);
}

function uploadArduino(done) {
  let pythonDir = glob.sync(`${dir.arduinoModules}/packages/esp8266/tools/python3/*`)[0]
  let uploadDir = glob.sync(`${dir.arduinoModules}/packages/esp8266/hardware/esp8266/*/tools/upload.py`)[0]
  let python = path.resolve(pythonDir + '/python');
  let upload = path.resolve(uploadDir);
  let bin = path.resolve(`${dir.build}/bin/${env.BIN_NAME}`);
  let cmd = `${python} ${upload} --chip esp8266 ${argv['port']? '--port ' + argv['port'] : ''} --baud 921600 --before default_reset --after hard_reset write_flash 0x0 "${bin}"`;

  let proc = child.exec(cmd).on('close', done);
  let logCB = function (buf) {
    process.stdout.write(buf);
  }
  proc.stdout.on('data', logCB);
  proc.stderr.on('data', logCB);
}

gulp.task('install', gulp.series(install));
gulp.task('compile', gulp.series(clean, compile));
gulp.task('upload', gulp.series(uploadArduino));

gulp.task('default', gulp.series('compile'));
